
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using MediatR;
using UserManagement.Application.Common.Behaviours;
using FluentValidation;

namespace UserManagement.Application;
    public static class DependencyInjection
    {
     
     public static IServiceCollection AddApplication(this IServiceCollection services){
         
        services.AddAutoMapper(Assembly.GetExecutingAssembly());

        services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
        services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
    
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));

        return services;
     }
    
    }