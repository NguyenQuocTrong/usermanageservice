﻿using System.Collections.Immutable;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using UserManagement.Application.Common.Results;

namespace UserManagement.Application.Common.Interfaces
{
    public interface IJwtAuthManager
    {
        IImmutableDictionary<string, RefreshToken> UsersRefreshTokensReadOnlyDictionary { get; }

        //gen token
        JwtAuthResult GenerateTokens(string username, Claim[] claims, DateTime now);

        //lam moi token
        JwtAuthResult Refresh(string refreshToken, string accessToken, DateTime now);
        void RemoveExpiredRefreshTokens(DateTime now);

        //xoa token
        void RemoveRefreshTokenByUserName(string userName);

        //giai ma token
        (ClaimsPrincipal, JwtSecurityToken) DecodeJwtToken(string token);
    }
}
