﻿using System.Text.Json.Serialization;
// phuc vu Genaraltoken khi tạo token cho user login
namespace UserManagement.Application.Common.Results
{
    public class JwtAuthResult
    {
        [JsonPropertyName("accessToken")]
        public string AccessToken { get; set; }

        [JsonPropertyName("refreshToken")]
        public RefreshToken RefreshToken { get; set; }
    }
}
