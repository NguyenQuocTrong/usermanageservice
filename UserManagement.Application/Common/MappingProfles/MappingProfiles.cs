﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Application.Common.Results;
using UserManagement.Domain.Common;

namespace UserManagement.Application.Common.MappingProfles
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
            
        {
            CreateMap<IdentityRole, RoleModel>().ReverseMap();
            CreateMap<ApplicationUser,UserResult>().ForMember(dest => dest.RoleName, opt => opt.Ignore()).ReverseMap();
        }



    }
}
