
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using UserManagement.Application.Common.Interfaces;
using UserManagement.Application.Common.Results;

namespace UserManagement.Application.ManageUser.Queries;


    public class GetRolesQuery : IRequest<List<RoleModel>>
    {
    }

    public class GetRolesQueryHandler : IRequestHandler<GetRolesQuery, List<RoleModel>>
    {
        private readonly ILogger<GetRolesQueryHandler> _logger;
        private readonly IMapper _mapper;
        private readonly IApplicationDbContext _context;

    public GetRolesQueryHandler( IMapper mapper, IApplicationDbContext context, ILogger<GetRolesQueryHandler> logger) {
           
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _context = context ?? throw new ArgumentNullException(nameof(context));
           _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

        public async Task<List<RoleModel>> Handle(GetRolesQuery request, CancellationToken cancellationToken = new CancellationToken())
        {
        _logger.LogInformation("Get roles successfully");

        return _mapper.Map<List<RoleModel>>(await _context.Roles.AsNoTracking().ToListAsync());
            
        }
    }
