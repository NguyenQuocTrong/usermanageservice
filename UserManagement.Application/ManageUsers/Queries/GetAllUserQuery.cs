
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using UserManagement.Application.Common.Interfaces;
using UserManagement.Application.Common.Results;

namespace UserManagement.Application.ManageUser.Queries;

public class GetAllUserQuery : IRequest<List<UserResult>>
{
}
public class GetAllUserQueryHandler : IRequestHandler<GetAllUserQuery, List<UserResult>>
{
    private readonly ILogger<GetAllUserQueryHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IIdentityService _identityService;

    public GetAllUserQueryHandler(ILogger<GetAllUserQueryHandler> logger, IMapper mapper, IIdentityService identityService)
    {
        _logger = logger;
        _mapper = mapper;
        _identityService = identityService;
    }

    public async Task<List<UserResult>> Handle(GetAllUserQuery request, CancellationToken cancellationToken = new CancellationToken())
    {
        var users = _mapper.Map<List<UserResult>>(await _identityService.GetUsersAsync());

        users.ForEach(i =>
        {
            i.RoleName = _identityService.GetRoleUserAsync(i.UserName).Result;
        });
        return users;
    }
}


