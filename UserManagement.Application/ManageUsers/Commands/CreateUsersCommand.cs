﻿using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Application.Common.Extensions;
using UserManagement.Application.Common.Interfaces;
using UserManagement.Application.Common.Models;
using UserManagement.Application.Common.Results;

namespace UserManagement.Application.ManageUsers.Commands
{
    /// <summary>
    ///List<CreateUserResult> is result controller call this
    /// </summary>
    public class CreateUsersCommand : IRequest<List<CreateUserResult>>
    {
        // pass data from controller
        public List<CreateUserRequest> Users { get; set; }
    }
    public class CreateUserCommandHandler : IRequestHandler<CreateUsersCommand, List<CreateUserResult>>
    {

        // Call DI
        private readonly IIdentityService _identityService;
        private readonly ILogger<CreateUserCommandHandler> _logger;
        public CreateUserCommandHandler(IIdentityService identityService, ILogger<CreateUserCommandHandler> logger)
        {

            _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<List<CreateUserResult>> Handle(CreateUsersCommand request, CancellationToken cancellationToken = new CancellationToken())
        {
            var createUserResults = new List<CreateUserResult>();
            foreach (var item in request.Users)
            {

                //call service Create User and pass
                (Result _result, string temporaryPassword) = await _identityService.CreateUserWithTemporaryPasswordAsync(item.Email, item.UserName, item.RoleId);

                if (!_result.Succeeded)
                {
                    _logger.LogError("Failed to create user with email {0}", item.Email);
                }
                createUserResults.Add(new CreateUserResult
                {
                    Email = item.Email,
                    UserName = item.UserName,
                    Password = temporaryPassword.ToBase64Encode()
                });
            }

            return createUserResults;
        }
    }

}
