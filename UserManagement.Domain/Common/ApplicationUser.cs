﻿using Microsoft.AspNetCore.Identity;


namespace UserManagement.Domain.Common
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() { }
        public bool RequireChangePassword { get; set; }
        public DateTime? LastModified { get; set; }
        public string? LastModifiedBy { get; set; }

    }
}
