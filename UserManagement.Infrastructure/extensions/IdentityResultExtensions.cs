﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Application.Common.Results;

namespace UserManagement.Infrastructure.Extensions
{
    public static class IdentityResultExtensions
    {
        // get value khi dung cac func Identity to model Result
        public static Result ToApplicationResult(this Microsoft.AspNetCore.Identity.IdentityResult result)
        {
            return result.Succeeded
                ? Result.Success()
                : Result.Failure(string.Join("<br/>", result.Errors));
        }
    }
}
