﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Net.WebSockets;
using System.Text;
using UserManagement.Application.Common.Interfaces;
using UserManagement.Application.Common.Results;
using UserManagement.Domain.Common;
using UserManagement.Infrastructure.Extensions;
using UserManagement.Infrastructure.Persistence;

namespace UserManagement.Infrastructure.Service
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;

        public IdentityService(
            ApplicationDbContext context,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager
            )
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
        }

        /// <summary>
        /// đăng kí role cho user ( dùng trong hàm tạo ng dùng)
        /// </summary>
        /// <param name="user"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public async Task<Microsoft.AspNetCore.Identity.IdentityResult> AssignUserToRole(ApplicationUser user, string role)
        {
            return await _userManager.AddToRoleAsync(user, role);
        }

        public Task<Result> ChangePasswordAsync(string userId, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

      
       /// <summary>
       /// Tạo người dùng trong hệ thống indentity
       /// </summary>
       /// <param name="email"></param>
       /// <param name="userName"></param>
       /// <param name="roleId"></param>
       /// <returns></returns>
       /// <exception cref="ArgumentNullException"></exception>
        public async Task<(Result, string)> CreateUserWithTemporaryPasswordAsync(string email, string userName, string roleId)
        {
          var user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                return (Result.Failure("User has been existed in system"), string.Empty);
            }

            //primary
            var temporaryPassword = CreatePassword(10);//4yzgL9OPet
            (Result result, string userId) = await CreateUserAsync(userName, temporaryPassword, false, email);

            if (result.Succeeded) {

               user = await _userManager.FindByIdAsync(userId);
               var role = await _roleManager.Roles.FirstOrDefaultAsync(x => x.Id.Equals(roleId));
               if (role == null)
                {
                    throw new ArgumentNullException(nameof(role));
                }
                await AssignUserToRole(user, role.Name);

            }

            return (result, temporaryPassword);

        }

        public Task<(Result, string)> GenerateNewPasswordAsync(string email)
        {
            throw new NotImplementedException();
        }

        public async Task<IList<string>> GetRolesUserAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            var roles = await _userManager.GetRolesAsync(user);
            return roles;
        }

        public async Task<string> GetRoleUserAsync(string userName)
        {
            var roles = await GetRolesUserAsync(userName);
            return roles.FirstOrDefault() ?? string.Empty;
        }

        /// <summary>
        /// suport login
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public async Task<ApplicationUser> GetUserByIdentifierAsync(string identifier)
        {
           var user = await _userManager.FindByIdAsync(identifier);
            if (user == null)
            {
                user = await _userManager.FindByNameAsync(identifier);

                if (user == null)
                {
                    user = await _userManager.FindByEmailAsync(identifier);
                }
            }
               
            return user;
        }

        public async Task<string> GetUserIdAsync(string userName)
        {
            var user = await _userManager.Users.FirstAsync(u => string.Equals(userName, u.UserName));
            return user.Id;
        }

        public async Task<List<ApplicationUser>> GetUsersAsync()
        {
            return await _context.Users.ToListAsync();
        }

        public Task LockUserAsync(string userId)
        {
            throw new NotImplementedException();
        }

       

        public Task UnlockUserAsync(string userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// login 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="rememberMe"></param>
        /// <returns></returns>
        public async Task<SignInResult> SignInAsync(string userName, string password, bool rememberMe)
        {
            return await _signInManager.PasswordSignInAsync(userName, password, rememberMe, lockoutOnFailure: false);
        }


        public async Task<(Result, string)> CreateUserAsync(
            string userName, 
            string password, 
            bool mustChangePassword = false, 
            string email = null)
        {
            var user = new ApplicationUser()
            {
                UserName = userName,
                Email = email ?? userName,
                LockoutEnabled = false,
                LockoutEnd = DateTime.Now,
                RequireChangePassword = mustChangePassword

            };
            var result = await _userManager.CreateAsync(user, password);
            return (result.ToApplicationResult(),user.Id );
        }



        // func support for this class
        internal string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]); //check valiation 
            }
            return res.ToString();
        }

        public async Task ResetAccessFailedCountAsync(string userId)
        {
            ApplicationUser user = new ApplicationUser()
            {
                Id = userId
            };
            await _userManager.ResetAccessFailedCountAsync(user);
        }
    }
}
