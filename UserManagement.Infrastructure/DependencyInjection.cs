﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UserManagement.Application.Common.Interfaces;
using UserManagement.Domain.Common;
using UserManagement.Infrastructure.Indentity;
using UserManagement.Infrastructure.Persistence;
using UserManagement.Infrastructure.Service;

namespace UserManagement.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services,
            IConfiguration configuration)
        {
           
            //cau hinh kết nối
            if(configuration.GetValue<bool>("UseInMemoryDatabase"))
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                 options.UseInMemoryDatabase("UseInMemoryDatabase"));
            }
            else
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    s => s.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));
            }

            //đăng kí DI lifetime cho kêt nối(scope)
            services.AddScoped<IApplicationDbContext, ApplicationDbContext>();

            //cấu hình role cho identity
            services.AddDefaultIdentity<ApplicationUser>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 0;

                options.SignIn.RequireConfirmedAccount = false;

                options.Lockout.AllowedForNewUsers = false;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromDays(36500);
                options.Lockout.MaxFailedAccessAttempts = 10;

            }).AddRoles<IdentityRole>()
              .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddTransient<IDateTime, DateTimeService>();
          

            //đăng kí DI lifeime cho role(scrope)
            services.AddTransient<IDateTime,DateTimeService>();

            services.AddTransient<IIdentityService, IdentityService>();
            

            services.AddHostedService<JwtRefreshTokenCache>();
            services.AddSingleton<IJwtAuthManager, JwtAuthManager>();

            return services;
        }

    }
}
