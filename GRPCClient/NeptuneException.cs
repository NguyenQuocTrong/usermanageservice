﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GRPCClient
{
    [Serializable]
    internal class NeptuneException : Exception
    {
        public NeptuneException()
        {
        }

        public NeptuneException(string message)
          : base(message)
        {
        }

        public NeptuneException(string message, Exception innerException)
          : base(message, innerException)
        {
        }

        protected NeptuneException(SerializationInfo info, StreamingContext context)
          : base(info, context)
        {
        }
    }
}
