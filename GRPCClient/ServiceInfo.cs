﻿using Grpc.Core;
using Grpc.Net.Client;
using GRPCClient.Protos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace GRPCClient
{
    public class ServiceInfo
    {
        private readonly long NEPTUNE_GRPC_TIMEOUT_IN_SECONDS = 60;

        [Key]
        public string service_code { get; set; }

        public string service_name { get; set; }

        public string service_grpc_url { get; set; }

        public string service_status { get; set; }

        public string service_grpc_active { get; set; }

        public long service_grpc_timeout_seconds { get; set; }

        public long service_ping_interval_seconds { get; set; }

        public string service_static_token { get; set; }

        public string broker_virtual_host { get; set; }

        public string broker_hostname { get; set; }

        public string broker_user_name { get; set; }

        public string broker_user_password { get; set; }

        public string broker_queue_name { get; set; }

        public string event_queue_name { get; set; }

        public int broker_port { get; set; }

        public string neptune_grpc_url { get; set; }

        public long neptune_server_time { get; set; }

        public long neptune_grpc_timeout_seconds { get; set; }

        public string ssl_active { get; set; }

        public string ssl_cert_pass_pharse { get; set; }

        public string ssl_cert_servername { get; set; }

        public string ssl_cert_base64 { get; set; }

        public long concurrent_threads { get; set; }

        public void QueryServiceInfo(string pFromServiceCode, string pToServiceCode)
        {
            MethodResult methodResult = new NeptuneGrpcService.NeptuneGrpcServiceClient((ChannelBase)GrpcChannel.
                ForAddress(GrpcClient.ClientConfig.NeptuneGrpcURL, new GrpcChannelOptions()
            {
                HttpHandler = (HttpMessageHandler)new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
                }
            })).Invoke(new MethodSpec()
            {
                FullClassName = "JITS.Neptune.Lib.GrpcLib.NeptuneServices",
                MethodName = nameof(QueryServiceInfo),
                Parameters = {
          pFromServiceCode,
          pToServiceCode
        }
            }, deadline: new DateTime?(DateTime.UtcNow.AddSeconds((double)this.NEPTUNE_GRPC_TIMEOUT_IN_SECONDS)));
            ServiceInfo serviceInfo = !methodResult.HasException ? JsonSerializer.Deserialize<ServiceInfo>(methodResult.ReturnValue) : throw new Exception(methodResult.ExceptionMessage);
            this.service_code = serviceInfo.service_code;
            this.service_name = serviceInfo.service_name;
            this.service_grpc_url = serviceInfo.service_grpc_url;
            this.service_status = serviceInfo.service_status;
            this.service_grpc_active = serviceInfo.service_grpc_active;
            this.service_grpc_timeout_seconds = serviceInfo.service_grpc_timeout_seconds;
            this.service_ping_interval_seconds = serviceInfo.service_ping_interval_seconds;
            this.service_static_token = serviceInfo.service_static_token;
            this.broker_virtual_host = serviceInfo.broker_virtual_host;
            this.broker_hostname = serviceInfo.broker_hostname;
            this.broker_user_name = serviceInfo.broker_user_name;
            this.broker_user_name = serviceInfo.broker_user_name;
            this.broker_user_password = serviceInfo.broker_user_password;
            this.broker_queue_name = serviceInfo.broker_queue_name;
            this.event_queue_name = serviceInfo.event_queue_name;
            this.broker_port = serviceInfo.broker_port;
            this.concurrent_threads = serviceInfo.concurrent_threads;
            this.ssl_active = serviceInfo.ssl_active;
            if (this.ssl_active.ToUpper().Equals("Y"))
            {
                this.ssl_cert_pass_pharse = serviceInfo.ssl_cert_pass_pharse;
                this.ssl_cert_servername = serviceInfo.ssl_cert_servername;
                this.ssl_cert_base64 = serviceInfo.ssl_cert_base64;
            }
            this.neptune_grpc_url = serviceInfo.neptune_grpc_url;
            this.neptune_server_time = serviceInfo.neptune_server_time;
            this.neptune_grpc_timeout_seconds = serviceInfo.neptune_grpc_timeout_seconds;
        }


        // customgrpc
        public void QueryServiceInfo(
             string pFromServiceCode,
             string host,
             string pInstanceID)
                {
            NeptuneGrpcService.NeptuneGrpcServiceClient grpcServiceClient = new NeptuneGrpcService.NeptuneGrpcServiceClient((ChannelBase)
                GrpcChannel.ForAddress(host, new GrpcChannelOptions()
            {
                HttpHandler = (HttpMessageHandler)new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
                }
            }));
            MethodSpec methodSpec = new MethodSpec();
            methodSpec.FullClassName = "IPCService.Service.IPCService";
            methodSpec.MethodName = nameof(QueryServiceInfo);
            methodSpec.Parameters.Add(pFromServiceCode);
            methodSpec.Parameters.Add(pInstanceID);
            MethodSpec request = methodSpec;
            DateTime? deadline = new DateTime?(DateTime.UtcNow.AddSeconds((double)this.NEPTUNE_GRPC_TIMEOUT_IN_SECONDS));
            CancellationToken cancellationToken = new CancellationToken();
            MethodResult methodResult = grpcServiceClient.Invoke(request, deadline: deadline, cancellationToken: cancellationToken);
            ServiceInfo serviceInfo = !methodResult.HasException ? JsonSerializer.Deserialize<ServiceInfo>(methodResult.ReturnValue) : throw new Exception(methodResult.ExceptionMessage);
            this.service_code = serviceInfo.service_code;
            this.service_name = serviceInfo.service_name;
            this.service_grpc_url = serviceInfo.service_grpc_url;
            this.service_status = serviceInfo.service_status;
            this.service_grpc_active = serviceInfo.service_grpc_active;
            this.service_grpc_timeout_seconds = serviceInfo.service_grpc_timeout_seconds;
            this.service_ping_interval_seconds = serviceInfo.service_ping_interval_seconds;
            this.service_static_token = serviceInfo.service_static_token;
            this.broker_virtual_host = serviceInfo.broker_virtual_host;
            this.broker_hostname = serviceInfo.broker_hostname;
            this.broker_user_name = serviceInfo.broker_user_name;
            this.broker_user_name = serviceInfo.broker_user_name;
            this.broker_user_password = serviceInfo.broker_user_password;
            this.broker_queue_name = serviceInfo.broker_queue_name;
            this.event_queue_name = serviceInfo.event_queue_name;
            this.broker_port = serviceInfo.broker_port;
            this.concurrent_threads = serviceInfo.concurrent_threads;
            this.ssl_active = serviceInfo.ssl_active;
            if (this.ssl_active.ToUpper().Equals("Y"))
            {
                this.ssl_cert_pass_pharse = serviceInfo.ssl_cert_pass_pharse;
                this.ssl_cert_servername = serviceInfo.ssl_cert_servername;
                this.ssl_cert_base64 = serviceInfo.ssl_cert_base64;
            }
            this.neptune_grpc_url = serviceInfo.neptune_grpc_url;
            this.neptune_server_time = serviceInfo.neptune_server_time;
            this.neptune_grpc_timeout_seconds = serviceInfo.neptune_grpc_timeout_seconds;
            //.broker_reconnect_interval_in_seconds = serviceInfo.broker_reconnect_interval_in_seconds;
        }

    }
   }

