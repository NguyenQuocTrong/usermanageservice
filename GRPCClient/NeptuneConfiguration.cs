﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace GRPCClient
{
    public class NeptuneConfiguration
    {
        private string _GUID= Guid.NewGuid().ToString();
        public string NeptuneGrpcURL { get; set; } = "";


        public string YourGrpcURL { get; set; }
        // thieu GetLocalip4

        public string YourServiceID { get; set; }

        public string YourInstanceID { get; set; }

        public NeptuneConfiguration Clone() => JsonSerializer.Deserialize<NeptuneConfiguration>(JsonSerializer.Serialize<NeptuneConfiguration>(this));

    }

}
