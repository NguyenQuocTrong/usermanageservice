﻿using Google.Protobuf;
using Google.Protobuf.Reflection;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRPCClient.Protos
{
    public sealed class CommandRequest : 
        IMessage<CommandRequest>,
        IMessage,
        IEquatable<CommandRequest>,
        IDeepCloneable<CommandRequest>,
        IBufferMessage
    {

        // Khai bao thuoc tinh

        // tạo mới 1 
        private static readonly MessageParser<CommandRequest> _parser = 
            new MessageParser<CommandRequest>((Func<CommandRequest>)(() => new CommandRequest()));
        
        private UnknownFieldSet _unknownFields;
        public const int NameFieldNumber = 1;
        private string name_ = "";

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public static MessageParser<CommandRequest> Parser => CommandRequest._parser;

        // [DebuggerNonUserCode]
        // [GeneratedCode("protoc", null)]
        // public static MessageDescriptor Descriptor => NeptuneGrpcServiceReflection.Descriptor.MessageTypes[2];

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public MessageDescriptor Descriptor => NeptuneGrpcServiceReflection.Descriptor.MessageTypes[2];

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public CommandRequest()
        {
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public CommandRequest(CommandRequest other)
          : this()
        {
            this.name_ = other.name_;
            this._unknownFields = UnknownFieldSet.Clone(other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public CommandRequest Clone() => new CommandRequest(this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public string Name
        {
            get => this.name_;
            set => this.name_ = ProtoPreconditions.CheckNotNull<string>(value, nameof(value));
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override bool Equals(object other) => this.Equals(other as CommandRequest);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public bool Equals(CommandRequest other)
        {
            if (other == null)
                return false;
            if (other == this)
                return true;
            return !(this.Name != other.Name) && object.Equals((object)this._unknownFields, (object)other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override int GetHashCode()
        {
            int hashCode = 1;
            if (this.Name.Length != 0)
                hashCode ^= this.Name.GetHashCode();
            if (this._unknownFields != null)
                hashCode ^= this._unknownFields.GetHashCode();
            return hashCode;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override string ToString() => JsonFormatter.ToDiagnosticString((IMessage)this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void WriteTo(CodedOutputStream output) => output.WriteRawMessage((IMessage)this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void InternalWriteTo(ref WriteContext output)
        {
            if (this.Name.Length != 0)
            {
                output.WriteRawTag(10);
                output.WriteString(this.Name);
            }
            if (this._unknownFields != null)
            {
                this._unknownFields.WriteTo(ref output);
            }
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public int CalculateSize()
        {
            int size = 0;
            if (this.Name.Length != 0)
                size += 1 + CodedOutputStream.ComputeStringSize(this.Name);
            if (this._unknownFields != null)
                size += this._unknownFields.CalculateSize();
            return size;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void MergeFrom(CommandRequest other)
        {
            if (other == null)
                return;
            if (other.Name.Length != 0)
                this.Name = other.Name;
            this._unknownFields = UnknownFieldSet.MergeFrom(this._unknownFields, other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void MergeFrom(CodedInputStream input) => input.ReadRawMessage((IMessage)this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void InternalMergeFrom(ref ParseContext input)
        {
            while (true)
            {
                uint num = input.ReadTag();
                uint num1 = num;
                if (num == 0)
                {
                    break;
                }
                if (num1 == 10)
                {
                    this.Name = input.ReadString();
                }
                else
                {
                    this._unknownFields = UnknownFieldSet.MergeFieldFrom(this._unknownFields, ref input);
                }
            }
        }



    }
}
