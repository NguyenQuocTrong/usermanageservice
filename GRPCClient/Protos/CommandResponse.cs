﻿using Google.Protobuf;
using Google.Protobuf.Reflection;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRPCClient.Protos
{

    public sealed class CommandResponse :
    IMessage<CommandResponse>,
    IMessage,
    IEquatable<CommandResponse>,
    IDeepCloneable<CommandResponse>,
    IBufferMessage
    {
        private static readonly MessageParser<CommandResponse> _parser = new MessageParser<CommandResponse>((Func<CommandResponse>)(() => new CommandResponse()));
        private UnknownFieldSet _unknownFields;
        public const int MessageFieldNumber = 2;
        private string message_ = "";

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public string Message
        {
            get
            {
                return this.message_;
            }
            set
            {
                this.message_ = ProtoPreconditions.CheckNotNull<string>(value, "value");
            }
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public MessageDescriptor Descriptor => NeptuneGrpcServiceReflection.Descriptor.MessageTypes[3];

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public static MessageParser<CommandResponse> Parser => new MessageParser<CommandResponse>((Func<CommandResponse>)(() => new CommandResponse()));


        // private static readonly MessageParser<CommandResponse> _parser = new MessageParser<CommandResponse>((Func<CommandResponse>) (() => new CommandResponse()));


        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public CommandResponse()
        {
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public CommandResponse(CommandResponse other) : this()
        {
            this.message_ = other.message_;
            this._unknownFields = UnknownFieldSet.Clone(other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public int CalculateSize()
        {
            int num = 0;
            if (this.Message.Length != 0)
            {
                num = num + 1 + CodedOutputStream.ComputeStringSize(this.Message);
            }
            if (this._unknownFields != null)
            {
                num += this._unknownFields.CalculateSize();
            }
            return num;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public CommandResponse Clone()
        {
            return new CommandResponse(this);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override bool Equals(object other)
        {
            return this.Equals(other as CommandResponse);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public bool Equals(CommandResponse other)
        {
            if (other == null)
            {
                return false;
            }
            if ((object)other == (object)this)
            {
                return true;
            }
            if (this.Message != other.Message)
            {
                return false;
            }
            return object.Equals(this._unknownFields, other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override int GetHashCode()
        {
            int hashCode = 1;
            if (this.Message.Length != 0)
            {
                hashCode ^= this.Message.GetHashCode();
            }
            if (this._unknownFields != null)
            {
                hashCode ^= this._unknownFields.GetHashCode();
            }
            return hashCode;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void MergeFrom(CommandResponse other)
        {
            if (other == null)
            {
                return;
            }
            if (other.Message.Length != 0)
            {
                this.Message = other.Message;
            }
            this._unknownFields = UnknownFieldSet.MergeFrom(this._unknownFields, other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void MergeFrom(CodedInputStream input)
        {
            input.ReadRawMessage(this);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        void IBufferMessage.InternalMergeFrom(ref ParseContext input)
        {
            while (true)
            {
                uint num = input.ReadTag();
                uint num1 = num;
                if (num == 0)
                {
                    break;
                }
                if (num1 == 18)
                {
                    this.Message = input.ReadString();
                }
                else
                {
                    this._unknownFields = UnknownFieldSet.MergeFieldFrom(this._unknownFields, ref input);
                }
            }
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        void IBufferMessage.InternalWriteTo(ref WriteContext output)
        {
            if (this.Message.Length != 0)
            {
                output.WriteRawTag(18);
                output.WriteString(this.Message);
            }
            if (this._unknownFields != null)
            {
                this._unknownFields.WriteTo(ref output);
            }
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override string ToString()
        {
            return JsonFormatter.ToDiagnosticString(this);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void WriteTo(CodedOutputStream output)
        {
            output.WriteRawMessage(this);
        }
    }
}

