﻿using Google.Protobuf;
using Google.Protobuf.Reflection;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRPCClient.Protos
{
    public sealed class MethodResult :
    IMessage<MethodResult>,
    IMessage,
    IEquatable<MethodResult>,
    IDeepCloneable<MethodResult>,
    IBufferMessage
    {

        private static readonly MessageParser<MethodResult> _parser = new MessageParser<MethodResult>((Func<MethodResult>)(() => new MethodResult()));
        private UnknownFieldSet _unknownFields;
        public const int ReturnValueFieldNumber = 1;
        private string returnValue_ = "";
        public const int ExecutionTimeMillisFieldNumber = 2;
        private long executionTimeMillis_;
        public const int HasExceptionFieldNumber = 3;
        private bool hasException_;
        public const int ExceptionMessageFieldNumber = 4;
        private string exceptionMessage_ = "";

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public static MessageParser<MethodResult> Parser => MethodResult._parser;

        // [DebuggerNonUserCode]
        // [GeneratedCode("protoc", null)]
        // public static MessageDescriptor Descriptor => NeptuneGrpcServiceReflection.Descriptor.MessageTypes[1];

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public MessageDescriptor Descriptor => NeptuneGrpcServiceReflection.Descriptor.MessageTypes[1];

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public MethodResult()
        {
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public MethodResult(MethodResult other)
          : this()
        {
            this.returnValue_ = other.returnValue_;
            this.executionTimeMillis_ = other.executionTimeMillis_;
            this.hasException_ = other.hasException_;
            this.exceptionMessage_ = other.exceptionMessage_;
            this._unknownFields = UnknownFieldSet.Clone(other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public MethodResult Clone() => new MethodResult(this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public string ReturnValue
        {
            get => this.returnValue_;
            set => this.returnValue_ = ProtoPreconditions.CheckNotNull<string>(value, nameof(value));
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public long ExecutionTimeMillis
        {
            get => this.executionTimeMillis_;
            set => this.executionTimeMillis_ = value;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public bool HasException
        {
            get => this.hasException_;
            set => this.hasException_ = value;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public string ExceptionMessage
        {
            get => this.exceptionMessage_;
            set => this.exceptionMessage_ = ProtoPreconditions.CheckNotNull<string>(value, nameof(value));
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override bool Equals(object other) => this.Equals(other as MethodResult);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public bool Equals(MethodResult other)
        {
            if (other == null)
                return false;
            if (other == this)
                return true;
            return !(this.ReturnValue != other.ReturnValue) && this.ExecutionTimeMillis == other.ExecutionTimeMillis && this.HasException == other.HasException && !(this.ExceptionMessage != other.ExceptionMessage) && object.Equals((object)this._unknownFields, (object)other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override int GetHashCode()
        {
            int hashCode = 1;
            if (this.ReturnValue.Length != 0)
                hashCode ^= this.ReturnValue.GetHashCode();
            if (this.ExecutionTimeMillis != 0L)
                hashCode ^= this.ExecutionTimeMillis.GetHashCode();
            if (this.HasException)
                hashCode ^= this.HasException.GetHashCode();
            if (this.ExceptionMessage.Length != 0)
                hashCode ^= this.ExceptionMessage.GetHashCode();
            if (this._unknownFields != null)
                hashCode ^= this._unknownFields.GetHashCode();
            return hashCode;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override string ToString() => JsonFormatter.ToDiagnosticString((IMessage)this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void WriteTo(CodedOutputStream output) => output.WriteRawMessage((IMessage)this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void InternalWriteTo(
          ref WriteContext output)
        {
            if (this.ReturnValue.Length != 0)
            {
                output.WriteRawTag((byte)10);
                output.WriteString(this.ReturnValue);
            }
            if (this.ExecutionTimeMillis != 0L)
            {
                output.WriteRawTag((byte)16);
                output.WriteInt64(this.ExecutionTimeMillis);
            }
            if (this.HasException)
            {
                output.WriteRawTag((byte)24);
                output.WriteBool(this.HasException);
            }
            if (this.ExceptionMessage.Length != 0)
            {
                output.WriteRawTag((byte)34);
                output.WriteString(this.ExceptionMessage);
            }
            if (this._unknownFields == null)
                return;
            this._unknownFields.WriteTo(ref output);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public int CalculateSize()
        {
            int size = 0;
            if (this.ReturnValue.Length != 0)
                size += 1 + CodedOutputStream.ComputeStringSize(this.ReturnValue);
            if (this.ExecutionTimeMillis != 0L)
                size += 1 + CodedOutputStream.ComputeInt64Size(this.ExecutionTimeMillis);
            if (this.HasException)
                size += 2;
            if (this.ExceptionMessage.Length != 0)
                size += 1 + CodedOutputStream.ComputeStringSize(this.ExceptionMessage);
            if (this._unknownFields != null)
                size += this._unknownFields.CalculateSize();
            return size;
        }

        MessageDescriptor IMessage.Descriptor => Descriptor;

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void MergeFrom(MethodResult other)
        {
            if (other == null)
                return;
            if (other.ReturnValue.Length != 0)
                this.ReturnValue = other.ReturnValue;
            if (other.ExecutionTimeMillis != 0L)
                this.ExecutionTimeMillis = other.ExecutionTimeMillis;
            if (other.HasException)
                this.HasException = other.HasException;
            if (other.ExceptionMessage.Length != 0)
                this.ExceptionMessage = other.ExceptionMessage;
            this._unknownFields = UnknownFieldSet.MergeFrom(this._unknownFields, other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void MergeFrom(CodedInputStream input) => input.ReadRawMessage((IMessage)this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void InternalMergeFrom(
          ref ParseContext input)
        {
            uint num;
            while ((num = input.ReadTag()) != 0U)
            {
                switch (num)
                {
                    case 10:
                        this.ReturnValue = input.ReadString();
                        continue;
                    case 16:
                        this.ExecutionTimeMillis = input.ReadInt64();
                        continue;
                    case 24:
                        this.HasException = input.ReadBool();
                        continue;
                    case 34:
                        this.ExceptionMessage = input.ReadString();
                        continue;
                    default:
                        this._unknownFields = UnknownFieldSet.MergeFieldFrom(this._unknownFields, ref input);
                        continue;
                }
            }
        }
    }
}
