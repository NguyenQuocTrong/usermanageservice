﻿using Google.Protobuf;
using Google.Protobuf.Collections;
using Google.Protobuf.Reflection;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRPCClient.Protos
{
    public sealed class MethodSpec :
    IMessage<MethodSpec>,
    IMessage,
    IEquatable<MethodSpec>,
    IDeepCloneable<MethodSpec>,
    IBufferMessage
    {

        private static readonly MessageParser<MethodSpec> _parser = new MessageParser<MethodSpec>((Func<MethodSpec>)(() => new MethodSpec()));
        private UnknownFieldSet _unknownFields;
        public const int FullClassNameFieldNumber = 1;
        private string fullClassName_ = "";
        public const int MethodNameFieldNumber = 2;
        private string methodName_ = "";
        public const int ParametersFieldNumber = 3;
        private static readonly FieldCodec<string> _repeated_parameters_codec = FieldCodec.ForString(26U);
        private readonly RepeatedField<string> parameters_ = new RepeatedField<string>();

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public static MessageParser<MethodSpec> Parser => MethodSpec._parser;
        //
        // [DebuggerNonUserCode]
        // [GeneratedCode("protoc", null)]
        // public static MessageDescriptor Descriptor => NeptuneGrpcServiceReflection.Descriptor.MessageTypes[0];

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        MessageDescriptor Descriptor => NeptuneGrpcServiceReflection.Descriptor.MessageTypes[0];

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public MethodSpec()
        {
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public MethodSpec(MethodSpec other)
          : this()
        {
            this.fullClassName_ = other.fullClassName_;
            this.methodName_ = other.methodName_;
            this.parameters_ = other.parameters_.Clone();
            this._unknownFields = UnknownFieldSet.Clone(other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public MethodSpec Clone() => new MethodSpec(this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public string FullClassName
        {
            get => this.fullClassName_;
            set => this.fullClassName_ = ProtoPreconditions.CheckNotNull<string>(value, nameof(value));
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public string MethodName
        {
            get => this.methodName_;
            set => this.methodName_ = ProtoPreconditions.CheckNotNull<string>(value, nameof(value));
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public RepeatedField<string> Parameters => this.parameters_;

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override bool Equals(object other) => this.Equals(other as MethodSpec);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public bool Equals(MethodSpec other)
        {
            if (other == null)
                return false;
            if (other == this)
                return true;
            return !(this.FullClassName != other.FullClassName) && !(this.MethodName != other.MethodName) && this.parameters_.Equals(other.parameters_) && object.Equals((object)this._unknownFields, (object)other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override int GetHashCode()
        {
            int num = 1;
            if (this.FullClassName.Length != 0)
                num ^= this.FullClassName.GetHashCode();
            if (this.MethodName.Length != 0)
                num ^= this.MethodName.GetHashCode();
            int hashCode = num ^ this.parameters_.GetHashCode();
            if (this._unknownFields != null)
                hashCode ^= this._unknownFields.GetHashCode();
            return hashCode;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public override string ToString() => JsonFormatter.ToDiagnosticString((IMessage)this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void WriteTo(CodedOutputStream output) => output.WriteRawMessage((IMessage)this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        void IBufferMessage.InternalWriteTo(
          ref WriteContext output)
        {
            if (this.FullClassName.Length != 0)
            {
                output.WriteRawTag((byte)10);
                output.WriteString(this.FullClassName);
            }
            if (this.MethodName.Length != 0)
            {
                output.WriteRawTag((byte)18);
                output.WriteString(this.MethodName);
            }
            this.parameters_.WriteTo(ref output, MethodSpec._repeated_parameters_codec);
            if (this._unknownFields == null)
                return;
            this._unknownFields.WriteTo(ref output);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public int CalculateSize()
        {
            int num = 0;
            if (this.FullClassName.Length != 0)
                num += 1 + CodedOutputStream.ComputeStringSize(this.FullClassName);
            if (this.MethodName.Length != 0)
                num += 1 + CodedOutputStream.ComputeStringSize(this.MethodName);
            int size = num + this.parameters_.CalculateSize(MethodSpec._repeated_parameters_codec);
            if (this._unknownFields != null)
                size += this._unknownFields.CalculateSize();
            return size;
        }

        MessageDescriptor IMessage.Descriptor => Descriptor;

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void MergeFrom(MethodSpec other)
        {
            if (other == null)
                return;
            if (other.FullClassName.Length != 0)
                this.FullClassName = other.FullClassName;
            if (other.MethodName.Length != 0)
                this.MethodName = other.MethodName;
            this.parameters_.Add((IEnumerable<string>)other.parameters_);
            this._unknownFields = UnknownFieldSet.MergeFrom(this._unknownFields, other._unknownFields);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void MergeFrom(CodedInputStream input) => input.ReadRawMessage((IMessage)this);

        [DebuggerNonUserCode]
        [GeneratedCode("protoc", null)]
        public void InternalMergeFrom(
          ref ParseContext input)
        {
            uint num;
            while ((num = input.ReadTag()) != 0U)
            {
                switch (num)
                {
                    case 10:
                        this.FullClassName = input.ReadString();
                        continue;
                    case 18:
                        this.MethodName = input.ReadString();
                        continue;
                    case 26:
                        this.parameters_.AddEntriesFrom(ref input, MethodSpec._repeated_parameters_codec);
                        continue;
                    default:
                        this._unknownFields = UnknownFieldSet.MergeFieldFrom(this._unknownFields, ref input);
                        continue;
                }
            }
        }
    }
}
