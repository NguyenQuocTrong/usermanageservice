﻿using Google.Protobuf;
using Grpc.Core;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GRPCClient.Protos
{
    public static class NeptuneGrpcService
    {
        private static readonly
       #nullable disable
       string __ServiceName = nameof(NeptuneGrpcService);

        [GeneratedCode("grpc_csharp_plugin", null)]
        private static readonly Marshaller<MethodSpec> __Marshaller_MethodSpec= Marshallers.Create<MethodSpec>(new Action<MethodSpec, SerializationContext>(NeptuneGrpcService.__Helper_SerializeMessage), (Func<DeserializationContext, MethodSpec>)(context => NeptuneGrpcService.__Helper_DeserializeMessage<MethodSpec>(context, MethodSpec.Parser)));

        [GeneratedCode("grpc_csharp_plugin", null)]
        private static readonly Marshaller<MethodResult> __Marshaller_MethodResult = Marshallers.Create<MethodResult>(new Action<MethodResult, SerializationContext>(NeptuneGrpcService.__Helper_SerializeMessage), (Func<DeserializationContext, MethodResult>)(context => NeptuneGrpcService.__Helper_DeserializeMessage<MethodResult>(context, MethodResult.Parser)));


        [GeneratedCode("grpc_csharp_plugin", null)]
        private static readonly Marshaller<CommandRequest> __Marshaller_CommandRequest = Marshallers.Create<CommandRequest>(new Action<CommandRequest, SerializationContext>(NeptuneGrpcService.__Helper_SerializeMessage), (Func<DeserializationContext, CommandRequest>)(context => NeptuneGrpcService.__Helper_DeserializeMessage<CommandRequest>(context, CommandRequest.Parser)));
        [GeneratedCode("grpc_csharp_plugin", null)]
        private static readonly Marshaller<CommandResponse> __Marshaller_CommandResponse = Marshallers.Create<CommandResponse>(new Action<CommandResponse, SerializationContext>(NeptuneGrpcService.__Helper_SerializeMessage), (Func<DeserializationContext, CommandResponse>)(context => NeptuneGrpcService.__Helper_DeserializeMessage<CommandResponse>(context, CommandResponse.Parser)));
       
        [GeneratedCode("grpc_csharp_plugin", null)]
        private static readonly Method<MethodSpec, MethodResult> __Method_Invoke = 
            new Method<MethodSpec, MethodResult>(
                MethodType.Unary, 
                NeptuneGrpcService.__ServiceName,
                "Invoke", 
                NeptuneGrpcService.__Marshaller_MethodSpec, 
                NeptuneGrpcService.__Marshaller_MethodResult
                );

        [GeneratedCode("grpc_csharp_plugin", null)]
        private static readonly Method<CommandRequest, CommandResponse> __Method_CommandServerStreaming = new Method<CommandRequest, CommandResponse>(
            MethodType.ServerStreaming,
            NeptuneGrpcService.__ServiceName,
            "CommandServerStreaming", 
            NeptuneGrpcService.__Marshaller_CommandRequest,
            NeptuneGrpcService.__Marshaller_CommandResponse
            );


        [GeneratedCode("grpc_csharp_plugin", null)]
        private static readonly Method<CommandRequest, CommandResponse> __Method_CommandClientStreaming = new Method<CommandRequest, CommandResponse>
            (MethodType.ClientStreaming,
            NeptuneGrpcService.__ServiceName, 
            "CommandClientStreaming", 
            NeptuneGrpcService.__Marshaller_CommandRequest,
            NeptuneGrpcService.__Marshaller_CommandResponse
            );

        [GeneratedCode("grpc_csharp_plugin", null)]
        private static readonly Method<CommandRequest, CommandResponse> __Method_CommandBiDirectionalStreaming = new Method<CommandRequest, CommandResponse>(
            MethodType.DuplexStreaming, 
            NeptuneGrpcService.__ServiceName,
            "CommandBiDirectionalStreaming",
            NeptuneGrpcService.__Marshaller_CommandRequest,
            NeptuneGrpcService.__Marshaller_CommandResponse
            );






        [GeneratedCode("grpc_csharp_plugin", null)]
        private static void __Helper_SerializeMessage(IMessage message, SerializationContext context)
        {
            if (message is IBufferMessage)
            {
                context.SetPayloadLength(message.CalculateSize());
                message.WriteTo(context.GetBufferWriter());
                context.Complete();
            }
            else
                context.Complete(message.ToByteArray());
        }

        [GeneratedCode("grpc_csharp_plugin", null)]
      private static T __Helper_DeserializeMessage<T>(
      DeserializationContext context,
      MessageParser<T> parser)
      where T : IMessage<T>
        {
            return NeptuneGrpcService.__Helper_MessageCache<T>.IsBufferMessage ? parser.ParseFrom(context.PayloadAsReadOnlySequence()) : parser.ParseFrom(context.PayloadAsNewBuffer());
        }

        [GeneratedCode("grpc_csharp_plugin", null)]
        private static class __Helper_MessageCache<T>
        {
            public static readonly bool IsBufferMessage = typeof(IBufferMessage).GetTypeInfo().IsAssignableFrom(typeof(T));
        }


        public class NeptuneGrpcServiceClient : ClientBase<NeptuneGrpcService.NeptuneGrpcServiceClient>
        {

            // contructor
            [GeneratedCode("grpc_csharp_plugin", null)]
            protected NeptuneGrpcServiceClient()
            {
            }


            [GeneratedCode("grpc_csharp_plugin", null)]
            public NeptuneGrpcServiceClient(ChannelBase channel)
            : base(channel)
            {
            }

            [GeneratedCode("grpc_csharp_plugin",null)]
            public NeptuneGrpcServiceClient(CallInvoker callInvoker) :base(callInvoker) { }


            [GeneratedCode("grpc_csharp_plugin", null)]
            protected NeptuneGrpcServiceClient(ClientBase.ClientBaseConfiguration configuration) : base(configuration) { }

            [GeneratedCode("grpc_csharp_plugin", null)]
            public virtual MethodResult Invoke(
                 MethodSpec request,
                 Metadata headers = null,
                 DateTime? deadline = null,
                 CancellationToken cancellationToken = default(CancellationToken))
            {
                return this.Invoke(request, headers, deadline, cancellationToken);
            }

            [GeneratedCode("grpc_csharp_plugin", null)]
            public virtual MethodResult Invoke(MethodSpec request, CallOptions options)
            {
                return this.CallInvoker.BlockingUnaryCall<MethodSpec, MethodResult>(NeptuneGrpcService.__Method_Invoke, (string)null, options, request);

            }

            [GeneratedCode("grpc_csharp_plugin", null)]
            public virtual AsyncUnaryCall<MethodResult> InvokeAsync(
               MethodSpec request,
               Metadata headers = null,
               DateTime? deadline = null,
               CancellationToken cancellationToken = default(CancellationToken))
               {
                return this.InvokeAsync(request, new CallOptions(headers, deadline, cancellationToken));
              }

            [GeneratedCode("grpc_csharp_plugin", null)]
            public virtual AsyncUnaryCall<MethodResult> InvokeAsync(
            MethodSpec request,
            CallOptions options)
            {
                return this.CallInvoker.AsyncUnaryCall<MethodSpec, MethodResult>(NeptuneGrpcService.__Method_Invoke, (string)null, options, request);
            }

            [GeneratedCode("grpc_csharp_plugin", null)]
             public virtual AsyncServerStreamingCall<CommandResponse> CommandServerStreaming(
                CommandRequest request,
                Metadata headers = null,
                DateTime? deadline = null,
                CancellationToken cancellationToken = default(CancellationToken))
                    {
                return this.CommandServerStreaming(request, new CallOptions(headers, deadline, cancellationToken));
            }



            [GeneratedCode("grpc_csharp_plugin", null)]
            public virtual AsyncServerStreamingCall<CommandResponse> CommandServerStreaming(
              CommandRequest request,
              CallOptions options)
            {
                return this.CallInvoker.AsyncServerStreamingCall<CommandRequest, CommandResponse>(NeptuneGrpcService.__Method_CommandServerStreaming, (string)null, options, request);
            }

            [GeneratedCode("grpc_csharp_plugin", null)]
            public virtual AsyncClientStreamingCall<CommandRequest, CommandResponse> CommandClientStreaming(
              Metadata headers = null,
              DateTime? deadline = null,
              CancellationToken cancellationToken = default(CancellationToken))
            {
                return this.CommandClientStreaming(new CallOptions(headers, deadline, cancellationToken));
            }

            [GeneratedCode("grpc_csharp_plugin", null)]
            public virtual AsyncClientStreamingCall<CommandRequest, CommandResponse> CommandClientStreaming(
              CallOptions options)
            {
                return this.CallInvoker.AsyncClientStreamingCall<CommandRequest, CommandResponse>(NeptuneGrpcService.__Method_CommandClientStreaming, (string)null, options);
            }

            [GeneratedCode("grpc_csharp_plugin", null)]
            public virtual AsyncDuplexStreamingCall<CommandRequest, CommandResponse> CommandBiDirectionalStreaming(
              Metadata headers = null,
              DateTime? deadline = null,
              CancellationToken cancellationToken = default(CancellationToken))
            {
                return this.CommandBiDirectionalStreaming(new CallOptions(headers, deadline, cancellationToken));
            }

            [GeneratedCode("grpc_csharp_plugin", null)]
            public virtual AsyncDuplexStreamingCall<CommandRequest, CommandResponse> CommandBiDirectionalStreaming(
              CallOptions options)
            {
                return this.CallInvoker.AsyncDuplexStreamingCall<CommandRequest, CommandResponse>(NeptuneGrpcService.__Method_CommandBiDirectionalStreaming, (string)null, options);
            }


            /// <summary>
            /// Implement by ClientBase
            /// </summary>
            /// <param name="configuration"></param>
            /// <returns></returns>
            /// <exception cref="NotImplementedException"></exception>
            [GeneratedCode("grpc_csharp_plugin", null)]
            protected override NeptuneGrpcService.NeptuneGrpcServiceClient NewInstance(
                   ClientBase.ClientBaseConfiguration configuration)
            {
                return new NeptuneGrpcService.NeptuneGrpcServiceClient(configuration);
            }
        }

 }
}
