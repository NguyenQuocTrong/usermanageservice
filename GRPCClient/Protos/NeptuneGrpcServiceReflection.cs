﻿using Google.Protobuf;
using Google.Protobuf.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRPCClient.Protos
{
    public static class NeptuneGrpcServiceReflection
    {
        private static FileDescriptor descriptor = FileDescriptor.FromGeneratedCode(Convert.FromBase64String
            ("Ch9Qcm90b3MvTmVwdHVuZUdycGNTZXJ2aWNlLnByb3RvIksKCk1ldGhvZFNw" + 
            "ZWMSFQoNRnVsbENsYXNzTmFtZRgBIAEoCRISCgpNZXRob2ROYW1lGAIgASgJ" + 
            "EhIKCnBhcmFtZXRlcnMYAyADKAkicAoMTWV0aG9kUmVzdWx0EhMKC1JldHVy" +
            "blZhbHVlGAEgASgJEhsKE0V4ZWN1dGlvblRpbWVNaWxsaXMYAiABKAMSFAoM" + 
            "SGFzRXhjZXB0aW9uGAMgASgIEhgKEEV4Y2VwdGlvbk1lc3NhZ2UYBCABKAki" + 
            "HgoOQ29tbWFuZFJlcXVlc3QSDAoEbmFtZRgBIAEoCSIiCg9Db21tYW5kUmVz" + 
            "cG9uc2USDwoHbWVzc2FnZRgCIAEoCTKAAgoSTmVwdHVuZUdycGNTZXJ2aWNl" + 
            "EiQKBkludm9rZRILLk1ldGhvZFNwZWMaDS5NZXRob2RSZXN1bHQSPQoWQ29t" + 
            "bWFuZFNlcnZlclN0cmVhbWluZxIPLkNvbW1hbmRSZXF1ZXN0GhAuQ29tbWFu" + 
            "ZFJlc3BvbnNlMAESPQoWQ29tbWFuZENsaWVudFN0cmVhbWluZxIPLkNvbW1h" + 
            "bmRSZXF1ZXN0GhAuQ29tbWFuZFJlc3BvbnNlKAESRgodQ29tbWFuZEJpRGly" + 
            "ZWN0aW9uYWxTdHJlYW1pbmcSDy5Db21tYW5kUmVxdWVzdBoQLkNvbW1hbmRS" + 
            "ZXNwb25zZSgBMAFCJKoCIUpJVFMuTmVwdHVuZS5OZXB0dW5lQ2xpZW50LlBy" +
            "b3Rvc2IGcHJvdG8z"), new FileDescriptor[0],
            new GeneratedClrTypeInfo((Type[])null, (Extension[])null, new GeneratedClrTypeInfo[4]
   {
      new GeneratedClrTypeInfo(typeof (MethodSpec), (MessageParser) MethodSpec.Parser, new string[3]
      {
        "FullClassName",
        "MethodName",
        "Parameters"
      }, (string[]) null, (Type[]) null, (Extension[]) null, (GeneratedClrTypeInfo[]) null),
      new GeneratedClrTypeInfo(typeof (MethodResult), (MessageParser) MethodResult.Parser, new string[4]
      {
        "ReturnValue",
        "ExecutionTimeMillis",
        "HasException",
        "ExceptionMessage"
      }, (string[]) null, (Type[]) null, (Extension[]) null, (GeneratedClrTypeInfo[]) null),
      new GeneratedClrTypeInfo(typeof (CommandRequest), (MessageParser) CommandRequest.Parser, new string[1]
      {
        "Name"
      }, (string[]) null, (Type[]) null, (Extension[]) null, (GeneratedClrTypeInfo[]) null),
      new GeneratedClrTypeInfo(typeof (CommandResponse), (MessageParser) CommandResponse.Parser, new string[1]
      {
        "Message"
      }, (string[]) null, (Type[]) null, (Extension[]) null, (GeneratedClrTypeInfo[]) null)
   }));
        public static FileDescriptor Descriptor => NeptuneGrpcServiceReflection.descriptor;


    }
}
