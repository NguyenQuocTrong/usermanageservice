﻿using Grpc.Core;
using Grpc.Net.Client;
using GRPCClient.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Z.Expressions;

namespace GRPCClient
{
    public class GrpcClient : IDisposable
    {

        private Timer timer;

        private object __lockServiceInfoObject= new object();


        private ServiceInfo __ServiceInfo = new ServiceInfo();

        private NeptuneConfiguration __ClientConfiguration = new NeptuneConfiguration();
        public static NeptuneConfiguration ClientConfig { get; set; } = new NeptuneConfiguration();

        /// <summary>
        /// clock to one thread acess.
        /// </summary>
        public ServiceInfo ServiceInfo 
        {
            get
            {
                lock (this.__lockServiceInfoObject)
                    return this.__ServiceInfo;
            }
            set
            {
                lock(this.__lockServiceInfoObject)
                     this.__ServiceInfo = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>

        public string ToServiceCode { get; set; }

        public GrpcClient()
        {
            this.__ClientConfiguration = GrpcClient.ClientConfig.Clone();
            this.ServiceInfo.QueryServiceInfo(this.__ClientConfiguration.YourServiceID, this.__ClientConfiguration.YourServiceID);
        }

        public GrpcClient(string host)
        {
            this.__ClientConfiguration = GrpcClient.ClientConfig.Clone();
            this.__ClientConfiguration.NeptuneGrpcURL = host;
            this.ServiceInfo.QueryServiceInfo(this.__ClientConfiguration.YourInstanceID, host,
             this.__ClientConfiguration.YourInstanceID);
        }


        /// <summary>
        ///  contructor 2 tham so
        /// </summary>
        /// <param name="pToServiceCode"></param>
        /// <param name="pClientConfig"></param>
        /// <exception cref="Exception"></exception>
        public GrpcClient(string pToServiceCode, NeptuneConfiguration pClientConfig)
        {
            this.__ClientConfiguration = pClientConfig.Clone();
            this.ToServiceCode = pToServiceCode;
            this.ServiceInfo.QueryServiceInfo(this.__ClientConfiguration.YourServiceID, this.ToServiceCode);
            if (this.ServiceInfo.service_code == null)
                throw new Exception("Service [" + pToServiceCode + "] is not found.");
        }



        public async Task<MethodResult> InvokeAsync(MethodSpec method)
        {
            long num = string.IsNullOrEmpty(this.ToServiceCode) ? this.ServiceInfo.neptune_grpc_timeout_seconds : this.ServiceInfo.service_grpc_timeout_seconds;
            return await this.InvokeAsync(method, num * 1000L);
        }

        public async Task<MethodResult> InvokeAsync(MethodSpec method, long pTimeout_millis)
        {
            MethodResult result = new MethodResult();
            try
            {
                if (this.ServiceInfo.service_grpc_active != null && !this.ServiceInfo.service_grpc_active.ToLower().Equals("active"))
                    throw new NeptuneException("Grpc is not active.");
                HttpClientHandler httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
                CallCredentials callCredentials = CallCredentials.FromInterceptor((AsyncAuthInterceptor)((context, meta) =>
                {
                    string str = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhdG9yIiwianRpIjoiOWNjY2ZjZGUtZjM4MC00MTdlLTk4ODEtMmM1MzQyNmIwYWM3IiwidWlkIjoiYWRtaW4iLCJyb2xlcyI6IkFkbWluaXN0cmF0b3IiLCJleHAiOjE2MzY5MzQ4ODgsImlzcyI6Ikp1c3QtSW4tVGltZSBTb2x1dGlvbnMgSlNDIiwiYXVkIjoiTmVwdHVuZSJ9.7wt96YP0iKjzM45K3yLc4w_RWUot40PTGTgZTbdySh0";
                    meta.Add("Authorization", "Bearer " + str);
                    return Task.CompletedTask;
                }));
                string address = string.IsNullOrEmpty(this.ToServiceCode) ? this.ServiceInfo.neptune_grpc_url : this.ServiceInfo.service_grpc_url;
                using (GrpcChannel channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions()
                {
                    HttpHandler = (HttpMessageHandler)httpClientHandler,
                    Credentials = ChannelCredentials.Create((ChannelCredentials)new SslCredentials(), callCredentials)
                }))
                {
                    result = await new NeptuneGrpcService.NeptuneGrpcServiceClient((ChannelBase)channel).InvokeAsync(method, deadline: new DateTime?(DateTime.UtcNow.AddMilliseconds((double)pTimeout_millis)));
                    Task.Run((Action)(() => this.__TrackGrpcOnNeptune(method, result)));
                }
            }
            catch (Exception ex)
            {
                result.HasException = true;
                result.ExceptionMessage = ex.Message;
            }
            return result;
        }



        private void __TrackGrpcOnNeptune(MethodSpec pCalledMethod, MethodResult pResult)
        {
            MethodSpec method = new MethodSpec();
            method.FullClassName = "JITS.Neptune.Lib.GrpcLib.NeptuneServices";
            method.MethodName = "TrackGrpc";
            method.Parameters.Add(GrpcClient.ClientConfig.YourInstanceID);
            if (string.IsNullOrEmpty(this.ToServiceCode))
                method.Parameters.Add("Neptune");
            else
                method.Parameters.Add(this.ToServiceCode);
            method.Parameters.Add(JsonSerializer.Serialize<MethodSpec>(pCalledMethod));
            method.Parameters.Add(JsonSerializer.Serialize<MethodResult>(pResult));
            this.InvokeNeptuneGrpcAsync(method);
        }


        public async Task<MethodResult> InvokeNeptuneGrpcAsync(MethodSpec method)
        {
            MethodResult methodResult;
            try
            {
                methodResult = await new NeptuneGrpcService.NeptuneGrpcServiceClient((ChannelBase)GrpcChannel.ForAddress(GrpcClient.ClientConfig.NeptuneGrpcURL, new GrpcChannelOptions()
                {
                    HttpHandler = (HttpMessageHandler)new HttpClientHandler()
                    {
                        ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
                    }
                })).InvokeAsync(method, deadline: new DateTime?(DateTime.UtcNow.AddSeconds((double)this.ServiceInfo.neptune_grpc_timeout_seconds)));
            }
            catch (Exception ex)
            {
                throw;
            }
            return methodResult;
        }



        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
