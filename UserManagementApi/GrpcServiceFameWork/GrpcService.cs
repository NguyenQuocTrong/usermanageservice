﻿using GRPCClient;
using GRPCClient.Protos;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
namespace UserManagement.Api.GrpcServiceFameWork
{
   
        public class GrpcService : IGrpcService
        {

            private string _host;
            public GrpcService(string host)
            {
                _host = host;
            }

            public async Task<T> Call<T>(string fullClassName, string methodName, params object[] parameters)
            {
                var obj = await CallGrpc();

                async Task<T> CallGrpc()
                {
                    GrpcClient client = new GrpcClient(_host);
                    if (client == null)
                        throw new Exception("failed to init grpc " + _host);
                    MethodSpec spec = new MethodSpec();
                    spec.FullClassName = fullClassName;
                    spec.MethodName = methodName;
                    object[] objArray = parameters;
                    for (int index = 0; index < objArray.Length; ++index)
                    {
                        obj = (T)objArray[index];
                        spec.Parameters.Add(TypeDescriptor.GetConverter(obj.GetType()).ConvertToInvariantString(obj));
                        // obj = (T)null;
                    }

                    objArray = (object[])null;
                    MethodResult result = await client.InvokeAsync(spec);
                    if (result.HasException)
                    {
                        throw new Exception(result.ExceptionMessage);
                    }

                    if (string.IsNullOrEmpty(result.ReturnValue))
                        return default(T);
                    if (typeof(T) == typeof(string))
                        return (T)Convert.ChangeType((object)result.ReturnValue, typeof(T));
                    try
                    {
                        T value = JsonConvert.DeserializeObject<T>(result.ReturnValue);
                        return value;
                    }
                    catch (Exception ex)
                    {
                           throw;
                    }

                    client = (GrpcClient)null;
                    spec = (MethodSpec)null;
                    result = (MethodResult)null;
                    T obj1;
                   return obj1;
                }

                return obj;
            }

       
    }
}
