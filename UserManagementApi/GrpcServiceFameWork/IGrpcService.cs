﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace UserManagement.Api.GrpcServiceFameWork
{
    public interface IGrpcService
    {
        Task<T> Call<T>(

       string fullClassName,
       string methodName,
       params object[] parameters);


    }
}
