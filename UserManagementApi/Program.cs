
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Serilog.Events;
using UserManagement.Domain.Common;
using UserManagement.Infrastructure.Persistence;

namespace UserManagement.Api
{
    public class Program
    {
        public async static Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();


            using var scope = host.Services.CreateScope();
            { 
            var services = scope.ServiceProvider;
            var hostingEnvironment = services.GetRequiredService<IWebHostEnvironment>();
            var logger = services.GetRequiredService<ILogger<Program>>();

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.RollingFile(Path.Combine(hostingEnvironment.ContentRootPath, "logs", $"log-{DateTime.Now:yyyyMMdd}.txt"))
                .CreateLogger();

            try
            {
                var context = services.GetRequiredService<ApplicationDbContext>();

                if (context.Database.IsSqlServer())
                {
                    context.Database.Migrate();
                }

                var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
                var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();

                await ApplicationDbContextSeed.SeedDefaultRolesAsync(roleManager);
                await ApplicationDbContextSeed.SeedDefaultUserAsync(userManager);
                await ApplicationDbContextSeed.SeedUserTestingAsync(userManager);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "An error occurred while migrating or seeding the database.");
                throw;
            }
        }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
           Host.CreateDefaultBuilder(args)
            .UseSerilog()
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();

            })
            .ConfigureAppConfiguration((hostingcontext, congfig) =>
            {   
                congfig
                .SetBasePath(hostingcontext.HostingEnvironment.ContentRootPath)
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                            .AddJsonFile($"appsettings.{hostingcontext.HostingEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: true)
                            .AddEnvironmentVariables();
           });
    }
}
