﻿using FluentValidation;
using FluentValidation.AspNetCore;
using GRPCClient;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NSwag;
using NSwag.Generation.Processors.Security;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using System.Configuration;
using System.Reflection;
using System.Text;
using UserManagement.Api.Filters;
using UserManagement.Api.GrpcServiceFameWork;
using UserManagement.Application;
using UserManagement.Application.Common.Interfaces;
using UserManagement.Infrastructure;
using UserManagement.Infrastructure.Indentity;
using UserManagement.Infrastructure.Service;

using OpenApiSecurityScheme = NSwag.OpenApiSecurityScheme;

namespace UserManagement.Api;

   //
    public class Startup
    {

    public IConfiguration Configuration { get; }
    public Startup(IConfiguration configuration)
        {
            Configuration = configuration;// nhận nhiều nguồn config khác nhau dev,producctio,...
          
    }

       

        
        public void ConfigureServices(IServiceCollection services )
        {

        //them comtroller và validation by fulenvalidation, custom exceptions lại.
        //.AddFluentValidation(); now .net 6.0 had install o the controller pipeline in ASP.NET Core 6.0 
        //  Flow 1 Piple 
        // services.AddControllers(options =>
        //        options.Filters.Add(new ApiExceptionFilterAttribute())); //attribute to the global filter collection for controllers


        

        services.AddControllers();//attribute to the global filter collection for controllers


        // filter dommain được trỏ vô setting.json
        var validDomains = Configuration.GetSection("ValidDomains").GetChildren().Select(x => x.Value).ToArray();
        services.AddCors(options =>
        {
            options.AddPolicy(name: "SHOSE69",
            builder =>
            {
                builder.WithOrigins(validDomains)
                                    .AllowAnyHeader()
                                    .AllowAnyMethod()
                                    .AllowAnyOrigin();
            });
        });


        services.AddMemoryCache();
        services.AddOpenApiDocument(configure =>
            {
                configure.Title = "UserManagement API";

                configure.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Type into the textbox: Bearer {your JWT token}.",
                });

                configure.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("JWT"));
            });

        services.AddSingleton<ICurrentUserService, CurrentUserService>(); // Asign service
        services.AddInfrastructure(Configuration); //lien quan Entity
        services.AddApplication();
        services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));

        //chuyển đổi tất cả các phần tử trong các URL thành chữ thường
        services.AddRouting(options => options.LowercaseUrls = true);


        ConfigureJWT(services);
    }

    private void ConfigureJWT(IServiceCollection services)
    {
        var jwtTokenConfig = Configuration.GetSection("JWTTokenConfig").Get<JwtTokenConfig>();
        services.AddScoped<IGrpcService, GrpcService>();
        services.AddSingleton(jwtTokenConfig);
        services.AddAuthentication(x =>
        {
            x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(x =>
        {
            x.RequireHttpsMetadata = true;
            x.SaveToken = true;
            x.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtTokenConfig.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtTokenConfig.Secret)),
                ValidAudience = jwtTokenConfig.Audience,
                ValidateAudience = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.FromMinutes(5)
            };
        });
    }


    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerfactory)
    {
        // if (!env.IsProduction())
        // {
        app.UseDeveloperExceptionPage();
        app.UseMigrationsEndPoint();
        app.UseOpenApi();
        app.UseRouting();

        app.UseSwaggerUI((options) =>
        {
           
            options.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
        });

        loggerfactory.AddSerilog();

        app.UseSerilogRequestLogging();

       // app.UseHealthChecks("/health");

       // app.UseHttpsRedirection();

        app.UseRouting();
        app.UseCors("SHOSE69");

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
   



    }

