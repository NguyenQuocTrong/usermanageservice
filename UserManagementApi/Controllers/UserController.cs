﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Application.Common.Interfaces;
using UserManagement.Application.Common.Model;
using UserManagement.Application.Common.Models;
using UserManagement.Application.Common.Results;
using UserManagement.Application.Users.Commands;
using UserManagement.Application.Users.Queries;

namespace UserManagement.Api.Controllers
{
    [Authorize]
    public class UserController : ApiController
    {
        private readonly ILogger<UserController> _logger;
        private readonly ICurrentUserService _currentUserService;

        public UserController(ILogger<UserController> logger,
            ICurrentUserService currentUserService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _currentUserService = currentUserService ?? throw new ArgumentNullException(nameof(currentUserService));
        }



        /// <summary>
        /// Register
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("register")]
        [ProducesResponseType(typeof(RegisterRequest), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Result), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Result), StatusCodes.Status200OK)]
        public async Task<ActionResult<Result>> Register([FromBody] RegisterRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(request);
            }
            Result result = await Mediator.Send(new AddUserCommand
            {
                Email = request.Email,
                Password = request.Password

            });
            if (result.Succeeded)
            {
                _logger.LogInformation($"User [{request.Email}] register successfully.");

                return Ok(result);
            }

            return BadRequest(Result.Failure("Register failed"));
        }

        /// <summary>
        /// Register
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("login")]
        [ProducesResponseType(typeof(LoginRequest), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(IdentityResult), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(IdentityResult), StatusCodes.Status200OK)]
        public async Task<ActionResult<IdentityResult>> Login([FromBody] LoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(request);
            }
            IdentityResult result = await Mediator.Send(new GetUserLoginResultQuery
            {
                UserName = request.UserName,
                Password = request.Password,
                RememberMe = request.RememberMe,

            });
            if (result == null)
                return Unauthorized();

            return Ok(result);

        }

        /// <summary>
        /// Logout => Delete Token
        /// </summary>
        /// <returns></returns>
        [HttpGet("logout")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> Logout() {

            var userName = User.Identity.Name;
            await Mediator.Send(new DeleteRefreshTokenCommand
            {
                UserName = userName
            }) ;
            _logger.LogInformation($"User [{userName}] logged out the system.");
            return Ok();


        }


    }
}
