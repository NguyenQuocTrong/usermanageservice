﻿using Microsoft.AspNetCore.Mvc;
using UserManagement.Api.Controllers;
using UserManagement.Application.Common.Interfaces;
using UserManagement.Application.Common.Models;
using UserManagement.Application.Common.Results;
using UserManagement.Application.ManageUser.Queries;
using UserManagement.Application.ManageUsers.Commands;
using UserManagement.Domain.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using System.Data;

namespace AdminController.Api.Controllers;

/// <summary>
/// Have need Bearr jwt have Role SystemAdministrator,ITAdministrator
/// </summary>
[Authorize(Roles = "SystemAdministrator,ITAdministrator")]
public class AdminController : ApiController
    {
        private readonly ILogger<AdminController> _logger;
        private readonly ICurrentUserService _currentUserService;

        public AdminController(ILogger<AdminController> logger,
            ICurrentUserService currentUserService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _currentUserService = currentUserService ?? throw new ArgumentNullException(nameof(currentUserService));
        }

    /// <summary>
    /// Get All User have Rolle  SystemAdministrator,ITAdministrator
    /// </summary>
    /// <returns></returns>
        [HttpGet("users")]
        [ProducesResponseType(typeof(List<UserResult>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<List<UserResult>>> RetriveAllUsersAsync()
        {
            var users = await Mediator.Send(new GetAllUserQuery { });
            users.RemoveAll(i => i.Id == _currentUserService.UserId);
            return Ok(users);
        }

        [HttpGet("roles")]
        [ProducesResponseType(typeof(List<RoleModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<List<RoleModel>>> GetRolesAsync()
        {
            var roles = await Mediator.Send(new GetRolesQuery { });
            roles.RemoveAll(i => i.Name.Equals(Roles.SystemAdministrator));
            return Ok(roles);
        }

    /// <summary>
    /// CreateUserAsync 
    /// </summary>
    [HttpPost("users")]
    [ProducesResponseType(typeof(List<CreateUserResult>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(List<CreateUserResult>), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult<List<CreateUserResult>>> CreateUserAsync([FromBody] List<CreateUserRequest> createUsersRequest)
    {
        
        if (!ModelState.IsValid)
        {
            return BadRequest(createUsersRequest);
        }

         return Ok(await Mediator.Send(new CreateUsersCommand { Users = createUsersRequest }));
       
    }



}
