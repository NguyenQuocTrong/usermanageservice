﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Application.Common.Results;
using UserManagement.Application.ManageUser.Queries;

namespace UserManagement.Api.Controllers
{
    [ApiController]
    [Route("/api/user/[controller]")]
    public class ApiController : ControllerBase
    {

        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

    }
}
